package com;
public class EmployeeA implements IEmployee {

    protected String name;

    public EmployeeA(String name){
        this.name = name;
    }

    @Override
    public void doing(String task) {
        System.out.println("我是员工A,我擅长"+name+",现在开始做"+task+"工作");
    }
}
