package jdkproxy;
import staticproxy.IPerson;
import staticproxy.WangWu;

public class JdkProxyTest {
    public static void main(String[] args) {
        JdkMeiPoProxy jdkMeiPoProxy = new JdkMeiPoProxy();
        IPerson wangwu = jdkMeiPoProxy.instance(new WangWu());
        wangwu.findLove();
        System.out.println("======================================");
        IPerson zhaoliu = jdkMeiPoProxy.instance(new ZhaoLiu());
        zhaoliu.findLove();
    }
}
