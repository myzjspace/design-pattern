package com;
/**
 *策略角色(也可以是接口)
 */
public abstract class Strategy {

    /**
     * 算法方法
     */
    public abstract void algorithm();

}
