package com;

import java.util.ArrayList;
import java.util.List;

public class DeepCopyTest {
    public static void main(String[] args) {
        //创建原型对象
        ConcretePrototype prototype = new ConcretePrototype();
        prototype.setAge(11);
        prototype.setName("张三");
        List<String> houses = new ArrayList<>();
        houses.add("北京房产");
        houses.add("上海房产");
        prototype.setHouses(houses);
        System.out.println("原对象: "+prototype);

        //拷贝原型对象
        ConcretePrototype cloneType = prototype.deepClone();
        cloneType.getHouses().add("广州房产");

        System.out.println("原型对象: "+prototype);
        System.out.println("克隆对象: "+cloneType);
        System.out.println(prototype == cloneType);

        System.out.println("原型对象的房子: "+prototype.getHouses());
        System.out.println("克隆对象的房子: "+cloneType.getHouses());
        System.out.println(prototype.getHouses() == cloneType.getHouses());
    }
}
