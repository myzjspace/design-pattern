package design.adapter.classadapter;
/**
 * 测试类
 */
public class AdapterTest {
    public static void main(String[] args) {
        //使用普通功能类
        Target concreteTarget = new ConcreteTarget();
        concreteTarget.request();

        //使用特殊功能类,即适配类
        Target adapter = new Adapter();
        adapter.request();

        System.out.println("111111111111111111111111111");
    }
}
