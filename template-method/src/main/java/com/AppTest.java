package com;
/**
 * 模板方法模式是指定义一个操作中的算法的框架,而将一些步骤延迟到子类中。
 * 使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤,属于行为型设计模式。
 */
public class AppTest 
{
    public static void main(String[] args) {
        MeCook cook = new MeCook();
        cook.setIsAddOil(false);
        cook.cook();
        //大厨做饭放油是默认的,不用指定
        Cook cook1 = new ChefCook();
        cook1.cook();
    }
}
