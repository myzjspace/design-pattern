package com;
/**
 * 产品
 */
public class Project {

    private String demand;

    private String framework;

    private String outline;

    private String test;

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getFramework() {
        return framework;
    }

    public void setFramework(String framework) {
        this.framework = framework;
    }

    public String getOutline() {
        return outline;
    }

    public void setOutline(String outline) {
        this.outline = outline;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    @Override
    public String toString() {
        return "Project{" +
                "demand='" + demand + '\'' +
                ", framework='" + framework + '\'' +
                ", outline='" + outline + '\'' +
                ", test='" + test + '\'' +
                '}';
    }
}
