package general;

/**
 * 真实主题角色
 */
public class RealSubject implements ISubject {

    @Override
    public void request() {
        System.out.println("调用真实主题角色");
    }
}
