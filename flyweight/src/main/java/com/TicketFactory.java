package com;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TicketFactory {

    //缓存也是享元的一种
    private static Map<String,ITicket> pool = new ConcurrentHashMap<>();

    public static ITicket queryTicket(String from,String to){
        String key = from +"->"+to;
        if(pool.containsKey(key)){
            System.out.println("使用缓存的key:"+key);
            return pool.get(key);
        }
        System.out.println("第一次查询创建对象:"+key);
        ITicket iTicket = new TrainTicket(from,to);
        pool.put(key,iTicket);
       return iTicket;
    }
}
