package com;
/**
 * 委派模式(Delegate Pattern)又叫委托模式,是一种面向对象的设计模式,允许对象组合实现与继承相同的代码重用。
 * 他的基本作用就是负责任务的调用和分配任务,是一种特殊的静态代理,可以理解为全权代理,但是代理模式注重过程,而委派模式注重结果。
 * 委派模式属于行为型模式,不属于GOF23种设计模式中。
 */
public class Boss {

    public static void main(String[] args) {
        new Leader().doing("登录");
        new Leader().doing("登录页面");
    }

}
