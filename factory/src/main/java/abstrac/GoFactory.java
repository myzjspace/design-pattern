package abstrac;

public class GoFactory implements StudyFactory {
    @Override
    public INote studyNote() {
        return new GoNote();
    }

    @Override
    public IVideo studyVideo() {
        return new GoViedo();
    }
}
