package com;
/**
 * 将定义的算法家族、分别封装起来,让它们之间可以相互替换,从而让算法的变化不会影响到使用算法的用户。
 */
public class AppTest 
{
    public static void main(String[] args) {
        Context context;
        context = new Context(new ConcreteStrategyA());
        context.context();

        context = new Context(new ConcreteStrategyB());
        context.context();

        context = new Context(new ConcreteStrategyC());
        context.context();


    }
}
