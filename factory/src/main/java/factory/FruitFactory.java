package factory;
import simple.Fruit;

public interface FruitFactory {
    Fruit createFruit();
}
