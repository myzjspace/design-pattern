package com.testone;

public class SituationB {
    /**
     * 请求支援
     * @param situation
     */
    public void requestSupport(String situation){
        System.out.println(getClass().getSimpleName() + "：这里是B战区，现在被敌方攻打，请求" + situation + "支援");
    }

    /**
     * 是否支援
     * @param isSupport
     */
    public void support(boolean isSupport) {
        if (isSupport) {
            System.out.println(getClass().getSimpleName() + "：Copy that，还有五秒钟到达战场");
        } else {
            System.out.println(getClass().getSimpleName() + "：支援你妹，我也正在被攻打");
        }
    }
}
