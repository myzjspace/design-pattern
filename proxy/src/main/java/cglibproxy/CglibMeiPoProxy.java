package cglibproxy;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import java.lang.reflect.Method;

public class CglibMeiPoProxy implements MethodInterceptor {

    public Object getInstance(Class<?> clazz){
        Enhancer enhancer = new Enhancer();
        //把父类设置为谁？
        //这一步就是告诉cglib,生成的子类需要继承哪个类
        enhancer.setSuperclass(clazz);
        //设置拦截器CglibMeiPoProxy
        enhancer.setCallback(this);
        //创建并返回代理对象
        return enhancer.create();
    }

    /**
     *
     * @param sub cglib生成的代理对象
     * @param method 被代理对象方法
     * @param objects 方法入参
     * @param methodProxy 代理方法
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object sub, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        before();
        Object obj = methodProxy.invokeSuper(sub,objects);
        after();
        return obj;
    }

    private void before() {
        System.out.println("我是媒婆,现在开始给你找对象了,根据你的要求");
        System.out.println("开始查找对象");
    }

    private void after() {
        System.out.println("如果合适的话,牵手成功");
    }
}
