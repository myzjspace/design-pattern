package com;
/**
 * 抽象建造类
 */
public abstract class Builder {

    protected Project project = new Project();

    /**
     * 需求分析
     */
    public abstract Builder demand();

    /**
     * 架构设计
     */
    public abstract Builder framework();

    /**
     * 概要设计
     */
    public abstract Builder outline();

    /**
     * 集成测试
     */
    public abstract Builder test();

    /**
     * 得到产品
     * @return
     */
    public Project getProject(){
        return project;
    }



}
