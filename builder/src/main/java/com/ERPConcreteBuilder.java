package com;

/**
 * 具体建造者
 */
public class ERPConcreteBuilder extends Builder{

    @Override
    public Builder demand() {
       project.setDemand("ERP 需求阶段...");
       System.out.println("ERP 需求阶段...");
       return this;
    }

    @Override
    public Builder framework() {
        project.setFramework("ERP 架构设计阶段...");
        System.out.println("ERP 架构设计阶段...");
        return this;
    }

    @Override
    public Builder outline() {
        project.setOutline("ERP 概要设计阶段...");
        System.out.println("ERP 概要设计阶段...");
        return this;
    }

    @Override
    public Builder test() {
        project.setTest("ERP 集成测试阶段...");
        System.out.println("ERP 集成测试阶段...");
        return this;
    }

    @Override
    public Project getProject() {
        return super.getProject();
    }
}
