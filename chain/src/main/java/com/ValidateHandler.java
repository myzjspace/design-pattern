package com;

public class ValidateHandler extends Handler {

    @Override
    public void doHandle(Member member) {
        if(validateName(member.getName()) || validatePass(member.getPass())){
            System.out.println("用户名或者密码为空");
            return;
        }
        System.out.println("用户名和密码校验成功,允许向下执行");
        chain.doHandle(member);
    }

    private boolean validateName(String name){
        return (name == null || name.length()==0);
    }

    private boolean validatePass(String pass){
        return (pass == null || pass.length()==0);
    }

}
