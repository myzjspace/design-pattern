package com;
/**
 * 具体建造者
 */
public class ExamSystemConcreteBuilder extends Builder{

    @Override
    public Builder demand() {
       project.setDemand("考试系统 需求阶段...");
       System.out.println("考试系统 需求阶段...");
       return this;
    }

    @Override
    public Builder framework() {
        project.setFramework("考试系统 架构设计阶段...");
        System.out.println("考试系统 架构设计阶段...");
        return this;
    }

    @Override
    public Builder outline() {
        project.setOutline("考试系统 概要设计阶段...");
        System.out.println("考试系统 概要设计阶段...");
        return this;
    }

    @Override
    public Builder test() {
        project.setTest("考试系统 集成测试阶段...");
        System.out.println("考试系统 集成测试阶段...");
        return this;
    }

    @Override
    public Project getProject() {
        return super.getProject();
    }
}
