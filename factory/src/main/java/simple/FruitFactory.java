package simple;
public class FruitFactory {

    public static Fruit createFruit(String type){
        if(type.equals("apple")){
           return new Apple();
         }else if(type.equals("pear")){
          return new Pear();
        }else {
            System.out.println("没有找到");
        }
        return null;
    }

    public static Fruit createNewFruit(Class<? extends Fruit> clazz){
        try {
            if(clazz != null){
              return clazz.newInstance();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
