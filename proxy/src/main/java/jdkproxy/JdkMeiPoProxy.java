package jdkproxy;
import staticproxy.IPerson;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
/**
 * 媒婆类
 */
public class JdkMeiPoProxy implements InvocationHandler {

    private Object target;

    public IPerson instance(Object target){
        this.target = target;
        Class<?> clazz = target.getClass();
       return (IPerson) Proxy.newProxyInstance(clazz.getClassLoader(),clazz.getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before();
       Object result = method.invoke(target,args);
        after();
        return result;
    }

    private void before() {
        System.out.println("媒婆已经开始收集需求了,开始寻找中");
    }

    private void after() {
        System.out.println("双方同意,开始交往");
    }
}
