package factory;
/**
 * 工厂方法模式是指定义一个用于创建对象的接口,但让实现这个接口的类来决定实例化哪一个类在工厂方法模式。
 * Factory Method使一个类的实例化延迟到其子类。在工厂方法模式中用户只需要关心所需产品对应的工厂,
 * 无须关心创建细节。
 */
public class FactoryTest {
    public static void main(String[] args) {
        FruitFactory fruitFactory = new AppleFactory();
        fruitFactory.createFruit().whatIm();
    }
}
