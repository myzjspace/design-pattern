package abstrac;
/**
 * 抽象工厂模式(Abastract Factory Pattern)是指提供一个创建一系列相关或相互依赖对象的接口,
 * 无须指定他们具体的类。
 */
public class AbstractTest {
    public static void main(String[] args) {
        JavaFactory javaFactory = new JavaFactory();
        javaFactory.studyNote().edit();
        javaFactory.studyVideo().record();
    }
}
