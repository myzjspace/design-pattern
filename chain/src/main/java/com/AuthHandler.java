package com;

public class AuthHandler extends Handler {
    @Override
    public void doHandle(Member member) {
        if(!"超级管理员".equals(member.getRole())){
            System.out.println("您不是超级管理员,没有操作权限");
            return;
        }
        System.out.println("你是超级管理员,可以操作");
    }
}
