package general;
/**
 * 代理主题角色
 */
public class Proxy implements ISubject {

    private ISubject subject;

    public Proxy(ISubject subject) {
        this.subject = subject;
    }

    @Override
    public void request() {
        before();
        subject.request();
        after();
    }

    private void before() {
        System.out.println("===============方法前执行===============");
    }

    private void after() {
        System.out.println("===============方法后执行===============");
    }
}
