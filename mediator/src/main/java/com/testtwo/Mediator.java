package com.testtwo;

import com.testone.SituationA;
import com.testone.SituationB;
import com.testone.SituationC;

public abstract class Mediator {
    protected SituationA situationA;
    protected SituationB situationB;
    protected SituationC situationC;

    Mediator(){
        situationA = new SituationA();
        situationB = new SituationB();
        situationC = new SituationC();
    }

    /**
     * 事件的业务流程处理
     *
     * @param method
     */
    public abstract void execute(String method);

}
