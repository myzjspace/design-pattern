package staticproxy;

public class LiSi implements IPerson {

    private WangWu wangWu;

    public LiSi(WangWu wangWu) {
        this.wangWu = wangWu;
    }

    @Override
    public void findLove() {
        System.out.println("李四开始物色");
        wangWu.findLove();
        System.out.println("开始交往了");
    }
}
