package com.testone;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println("-------A被攻打，请求B支援--------");
        SituationA situationA = new SituationA();
        situationA.requestSupport("B");
        System.out.println("-------B也正在被攻打--------");
        SituationB situationB = new SituationB();
        situationB.support(false);
        System.out.println("-------A又向C请求支援--------");
        situationA.requestSupport("C");
        System.out.println("-------C很忙--------");
        SituationC situationC = new SituationC();
        situationC.support(false);
    }
}
