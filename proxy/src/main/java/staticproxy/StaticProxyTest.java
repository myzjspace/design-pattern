package staticproxy;

public class StaticProxyTest {
    public static void main(String[] args) {
        LiSi liSi = new LiSi(new WangWu());
        liSi.findLove();
    }
}
