package general;
/**
 * 代理主题角色
 */
public interface ISubject {
    void request();
}
