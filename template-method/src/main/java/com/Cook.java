package com;
/**
 *做饭
 */
public abstract class Cook {
    //加油
    public abstract void oil();

    //放鸡蛋
    public abstract void egg();

    //放西红柿
    public abstract void tomato();

    //钩子方法:让子类决定是否加油
    public boolean isAddOil(){
        return true;
    }

    //封装做饭方法
    //定义成final可以防止恶意操作
    public final void cook(){
       this.egg();
       this.tomato();
        //如果子类决定添加:则执行加油方法
       if(this.isAddOil()){
          this.oil();
       }
    }

}
