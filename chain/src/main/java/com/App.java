package com;
public class App 
{
    public static void main( String[] args )
    {
        //根据业务对handler进行串联起来,形成一个链
        login("admin","666");
    }

    public static void login(String name,String pass){
        ValidateHandler validateHandler = new ValidateHandler();
        LoginHandler loginHandler = new LoginHandler();
        AuthHandler authHandler = new AuthHandler();
        validateHandler.next(loginHandler);
        loginHandler.next(authHandler);
        validateHandler.doHandle(new Member(name,pass));
    }
}
