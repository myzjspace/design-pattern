package abstrac;

public interface StudyFactory {

    INote studyNote();

    IVideo studyVideo();


}
