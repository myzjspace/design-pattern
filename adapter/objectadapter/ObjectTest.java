package design.adapter.objectadapter;

import design.adapter.classadapter.Target;

public class ObjectTest {
    public static void main(String[] args) {
        //使用普通功能类
        Target target = new ConcreteTarget();
        target.request();

        //使用特殊功能类，即适配类，
        //需要先创建一个被适配类的对象作为参数
        Adapter adapter = new Adapter(new Adaptee());
        adapter.request();;
    }
}
