package abstrac;

public class JavaFactory implements StudyFactory {
    @Override
    public INote studyNote() {
        return new JavaNote();
    }

    @Override
    public IVideo studyVideo() {
        return new JavaViedo();
    }
}
