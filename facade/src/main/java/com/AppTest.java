package com;
/**
 * 门面模式又称外观模式.提供了一个统一的接口用来访问子系统的一群接口.
 * 主要特征是提供了一个高层接口,让子系统更容易使用,属于结构型模式
 */
public class AppTest {

    public static void main(String[] args) {
        FacadeService facadeService = new FacadeService();
        facadeService.doA();
        facadeService.doB();
        facadeService.doC();
    }
}
