package com.testone;

public class SituationA {

    public SituationA() {
    }

    /**
     * 请求支援
     * @param situation
     */
    public void requestSupport(String situation){
        System.out.println(getClass().getSimpleName() + "：这里是A战区，现在被敌方攻打，请求" + situation + "支援");
    }
}
