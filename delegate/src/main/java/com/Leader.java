package com;
import java.util.HashMap;
public class Leader implements IEmployee {

    HashMap<String,IEmployee> map = new HashMap<String,IEmployee>();

    public Leader(){
        map.put("登录",new EmployeeA("开发"));
        map.put("登录页面",new EmployeeB("UI"));
    }

    @Override
    public void doing(String task) {
       map.get(task).doing(task);
    }
}
