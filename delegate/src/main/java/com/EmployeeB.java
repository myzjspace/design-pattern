package com;

public class EmployeeB implements IEmployee {

    protected String name;

    public EmployeeB(String name){
        this.name = name;
    }
    @Override
    public void doing(String task) {
        System.out.println("我是员工B,我擅长"+name+",现在开始做"+task+"工作");
    }
}
