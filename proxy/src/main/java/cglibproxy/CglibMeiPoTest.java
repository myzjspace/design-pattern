package cglibproxy;

public class CglibMeiPoTest {
    public static void main(String[] args) {
        Customer customer = (Customer) new CglibMeiPoProxy().getInstance(Customer.class);
        customer.findLove();
    }
}
