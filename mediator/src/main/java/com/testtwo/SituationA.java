package com.testtwo;

/**
 * A战区
 */
public class SituationA extends Colleague {
    public SituationA(Mediator mediator) {
        super(mediator);
    }

    // 请求支援
    public void aRequestSupport() {
        super.mediator.execute("aRequestSupport");
    }
}
