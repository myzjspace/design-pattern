package com;
/**
 * 指挥者
 */
public class Director {

    public void ERPBuild(){
       Builder builder = new ERPConcreteBuilder();
       Project project = builder.demand().framework().outline().test().getProject();
       System.out.println(project);
    }

    public void ExamSystemBuild(){
        Builder builder = new ExamSystemConcreteBuilder();
        Project project = builder.demand().framework().outline().test().getProject();
        System.out.println(project);
    }
}
