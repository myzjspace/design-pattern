package factory;

import simple.Apple;
import simple.Fruit;

public class AppleFactory implements FruitFactory {
    @Override
    public Fruit createFruit() {
        return new Apple();
    }
}
